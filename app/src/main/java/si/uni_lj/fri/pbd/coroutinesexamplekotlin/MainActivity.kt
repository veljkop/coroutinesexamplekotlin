package si.uni_lj.fri.pbd.coroutinesexamplekotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import si.uni_lj.fri.pbd.coroutinesexamplekotlin.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val processingScope = CoroutineScope(Dispatchers.IO)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        binding.button.setOnClickListener {
            processingScope.launch { doProcessing() } }
    }

    private suspend fun doProcessing() {

        withContext(Dispatchers.Main) {
            binding.statusText.text = "Downloading..."
            binding.progressBar.visibility = View.VISIBLE
            binding.progressBar.progress = 0
        }

        for (i in 1..10) {
            try {
                Thread.sleep(1000)
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }

            val j = i
            withContext(Dispatchers.Main) {
                binding.progressBar.progress = j*10
            }
        }

        withContext(Dispatchers.Main) {
            binding.statusText.text = "All Done!"
            binding.progressBar.visibility = View.INVISIBLE
        }
    }
}